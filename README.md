# getBible API V2

Here you will find all the json files of all the **translations**, **books per/translation** and **chapters per/books per/translation** in the format of getBible V2

# How to use

Well you can clone/fork this repo and use it in any way you like in your projects. Even calling the files directly as raw from gitHub. But the basic idea was to be able to clone this repo to your web-server and then be able to have direct access to the files via your own domain/URL.

### We encourage you not to use getBible.net web-server any longer, but clone this repo to your own web-server, or use gitHub!
```
$ git clone https://gitlab.com/getbible/v2.git
```
> As an example of this repo in action try these few URLs. It will work the same on your own.

## As web-server file response
### translations
> get list of translations

_details_
[https://getbible.net/v2/translations.json](https://getbible.net/v2/translations.json)

_checksum_
[https://getbible.net/v2/checksum.json](https://getbible.net/v2/checksum.json)

> get a translation

_scripture_
[https://getbible.net/v2/kjv.json](https://getbible.net/v2/kjv.json)

_validation (checksum)_
[https://getbible.net/v2/kjv.sha](https://getbible.net/v2/kjv.sha)

### Books
> get list of books in translation

_details_
[https://getbible.net/v2/kjv/books.json](https://getbible.net/v2/kjv/books.json)

_checksum_
[https://getbible.net/v2/kjv/checksum.json](https://getbible.net/v2/kjv/checksum.json)

> get a book in translation

_scripture_
[https://getbible.net/v2/kjv/43.json](https://getbible.net/v2/kjv/43.json)

_validation (checksum)_
[https://getbible.net/v2/kjv/43.sha](https://getbible.net/v2/kjv/43.sha)

### Chapters
> get list of chapters in book in translation

_details_
[https://getbible.net/v2/kjv/43/chapters.json](https://getbible.net/v2/kjv/43/chapters.json)

_checksum_
[https://getbible.net/v2/kjv/43/checksum.json](https://getbible.net/v2/kjv/43/checksum.json)

> get a chapters in book in translation

_scripture_
[https://getbible.net/v2/kjv/43/1.json](https://getbible.net/v2/kjv/43/1.json)

_validation (checksum)_
[https://getbible.net/v2/kjv/43/1.sha](https://getbible.net/v2/kjv/43/1.sha)

## As gitHub file response
### translations
> get list of translations

_details_
[https://gitlab.com/getbible/v2/raw/master/translations.json](https://gitlab.com/getbible/v2/raw/master/translations.json)

_checksum_
[https://gitlab.com/getbible/v2/raw/master/checksum.json](https://gitlab.com/getbible/v2/raw/master/checksum.json)

> get a translation

_scripture_
[https://gitlab.com/getbible/v2/raw/master/kjv.json](https://gitlab.com/getbible/v2/raw/master/kjv.json)

_validation (checksum)_
[https://gitlab.com/getbible/v2/raw/master/kjv.sha](https://gitlab.com/getbible/v2/raw/master/kjv.sha)

### Books
> get list of books in translation

_details_
[https://gitlab.com/getbible/v2/raw/master/kjv/books.json](https://gitlab.com/getbible/v2/raw/master/kjv/books.json)

_checksum_
[https://gitlab.com/getbible/v2/raw/master/kjv/checksum.json](https://gitlab.com/getbible/v2/raw/master/kjv/checksum.json)

> get a book in translation

_scripture_
[https://gitlab.com/getbible/v2/raw/master/kjv/43.json](https://gitlab.com/getbible/v2/raw/master/kjv/43.json)

_validation (checksum)_
[https://gitlab.com/getbible/v2/raw/master/kjv/43.sha](https://gitlab.com/getbible/v2/raw/master/kjv/43.sha)

### Chapters
> get list of chapters in book in translation

_details_
[https://gitlab.com/getbible/v2/raw/master/kjv/43/chapters.json](https://gitlab.com/getbible/v2/raw/master/kjv/43/chapters.json)

_checksum_
[https://gitlab.com/getbible/v2/raw/master/kjv/43/checksum.json](https://gitlab.com/getbible/v2/raw/master/kjv/43/checksum.json)

> get a chapters in book in translation

_scripture_
[https://gitlab.com/getbible/v2/raw/master/kjv/43/1.json](https://gitlab.com/getbible/v2/raw/master/kjv/43/1.json)

_validation (checksum)_
[https://gitlab.com/getbible/v2/raw/master/kjv/43/1.sha](https://gitlab.com/getbible/v2/raw/master/kjv/43/1.sha)

# Heads-up

You can host these files on any web server like Apache or Ngnix. With Apache you may need to add the json type to the *httpd.conf* or *.htaccess*:
```
AddType application/json .json
```

#  License

The translations each have their own licencing but all are in the *public domain*. All licencing info are found in the full version of the transaltion and in the [https://getbible.net/v2/translations.json](https://getbible.net/v2/translations.json) file.

The API format is licensed under [GNU General Public License translation 2 or later](https://www.vdm.io/gnu-gpl).

# THE SCRIPTURE MAY NOT BE CHANGED!!!!

If you find errors in the text or if you are aware of any violations, please [contact us immediately](https://truechristian.church/contact-us).

> We have added checksum validation to all files for this reason to help you validate that the Holy Word of God before loading it.


