{
    "translation": "Open English Bible (Commonwealth Spelling)",
    "abbreviation": "oebcth",
    "lang": "en",
    "language": "English",
    "direction": "LTR",
    "encoding": "UTF-8",
    "book_nr": 59,
    "book_name": "James",
    "chapter": 2,
    "name": "James 2",
    "verses": [
        {
            "chapter": 2,
            "verse": 1,
            "name": "James 2:1",
            "text": "My friends, are you really trying to combine faith in Jesus Christ, our glorified Lord, with the worship of rank?"
        },
        {
            "chapter": 2,
            "verse": 2,
            "name": "James 2:2",
            "text": "Suppose a visitor should enter your synagogue, with gold rings and in grand clothes, and suppose a poor man should come in also, in shabby clothes,"
        },
        {
            "chapter": 2,
            "verse": 3,
            "name": "James 2:3",
            "text": "and you are deferential to the visitor who is wearing grand clothes, and say \u2014 \u201cThere is a good seat for you here,\u201d but to the poor man \u2014 \u201cYou must stand; or sit down there by my footstool,\u201d"
        },
        {
            "chapter": 2,
            "verse": 4,
            "name": "James 2:4",
            "text": "Is not that to make distinctions among yourselves, and show yourselves prejudiced judges?"
        },
        {
            "chapter": 2,
            "verse": 5,
            "name": "James 2:5",
            "text": "Listen, my dear friends. Has not God chosen those who are poor in the things of this world to be rich through their faith, and to possess the kingdom which he has promised to those who love him?"
        },
        {
            "chapter": 2,
            "verse": 6,
            "name": "James 2:6",
            "text": "But you \u2014 you insult the poor man! Is not it the rich who oppress you? Is not it they who drag you into law courts?"
        },
        {
            "chapter": 2,
            "verse": 7,
            "name": "James 2:7",
            "text": "Is not it they who malign that honourable name which has been bestowed on you?"
        },
        {
            "chapter": 2,
            "verse": 8,
            "name": "James 2:8",
            "text": "Yet, if you keep the royal law which runs \u2014 \u2018You must love your neighbour as you love yourself,\u2019 you are doing right;"
        },
        {
            "chapter": 2,
            "verse": 9,
            "name": "James 2:9",
            "text": "but, if you worship rank, you commit a sin, and stand convicted by that same law of being offenders against it."
        },
        {
            "chapter": 2,
            "verse": 10,
            "name": "James 2:10",
            "text": "For a person who has laid the law, as a whole, to heart, but has failed in one particular, is accountable for breaking all its provisions."
        },
        {
            "chapter": 2,
            "verse": 11,
            "name": "James 2:11",
            "text": "He who said \u2018You must not commit adultery\u2019 also said \u2018You must not murder.\u2019 If, then, you commit murder but not adultery, you are still an offender against the law."
        },
        {
            "chapter": 2,
            "verse": 12,
            "name": "James 2:12",
            "text": "Therefore, speak and act as people who are to be judged by the \u2018law of freedom.\u2019"
        },
        {
            "chapter": 2,
            "verse": 13,
            "name": "James 2:13",
            "text": "For there will be justice without mercy for the person who has not acted mercifully. Mercy triumphs over Justice."
        },
        {
            "chapter": 2,
            "verse": 14,
            "name": "James 2:14",
            "text": "  My friends, what is the good of a person\u2019s saying that they have faith, if they do not prove it by actions? Can such faith save them?"
        },
        {
            "chapter": 2,
            "verse": 15,
            "name": "James 2:15",
            "text": "Suppose some brother or sister should be in need of clothes and of daily bread,"
        },
        {
            "chapter": 2,
            "verse": 16,
            "name": "James 2:16",
            "text": "and one of you were to say to them \u2014 \u201cGo, and peace be with you; find warmth and food for yourselves,\u201d and yet you were not to give them the necessaries of life, what good would it be to them?"
        },
        {
            "chapter": 2,
            "verse": 17,
            "name": "James 2:17",
            "text": "In just the same way faith, if not followed by actions, is, by itself, a lifeless thing."
        },
        {
            "chapter": 2,
            "verse": 18,
            "name": "James 2:18",
            "text": "Some one, indeed, may say \u2014 \u201cYou are a man of faith, and I am a man of action.\u201d   \u201cThen show me your faith,\u201d I reply, \u201capart from any actions, and I will show you my faith by my actions.\u201d"
        },
        {
            "chapter": 2,
            "verse": 19,
            "name": "James 2:19",
            "text": "It is a part of your faith, is it not, that there is one God? Good; yet even the demons have that faith, and tremble at the thought."
        },
        {
            "chapter": 2,
            "verse": 20,
            "name": "James 2:20",
            "text": "Now do you really want to understand, fool, how it is that faith without actions leads to nothing?"
        },
        {
            "chapter": 2,
            "verse": 21,
            "name": "James 2:21",
            "text": "Look at our ancestor, Abraham. Was not it the result of his actions that he was pronounced righteous after he had offered his son, Isaac, on the altar?"
        },
        {
            "chapter": 2,
            "verse": 22,
            "name": "James 2:22",
            "text": "You see how, in his case, faith and actions went together; that his faith was perfected as the result of his actions;"
        },
        {
            "chapter": 2,
            "verse": 23,
            "name": "James 2:23",
            "text": "and that in this way the words of scripture came true \u2014 \u201cAbraham believed God, and that was regarded by God as righteousness,\u201d and \u201cHe was called the friend of God.\u201d"
        },
        {
            "chapter": 2,
            "verse": 24,
            "name": "James 2:24",
            "text": "You see, then, that it is as the result of their actions that a person is pronounced righteous, and not of their faith only."
        },
        {
            "chapter": 2,
            "verse": 25,
            "name": "James 2:25",
            "text": "Was not it the same with the prostitute, Rahab? Was not it as the result of her actions that she was pronounced righteous, after she had welcomed the messengers and helped them escape by?"
        },
        {
            "chapter": 2,
            "verse": 26,
            "name": "James 2:26",
            "text": "Exactly as a body is dead without a spirit, so faith is dead without actions.     "
        }
    ]
}