{
    "translation": "Weymouth NT",
    "abbreviation": "weymouth",
    "lang": "en",
    "language": "English",
    "direction": "LTR",
    "encoding": "",
    "book_nr": 45,
    "book_name": "Romans",
    "chapter": 7,
    "name": "Romans 7",
    "verses": [
        {
            "chapter": 7,
            "verse": 1,
            "name": "Romans 7:1",
            "text": "Brethren, do you not know--for I am writing to people acquainted with the Law--that it is during our lifetime that we are subject to the Law?"
        },
        {
            "chapter": 7,
            "verse": 2,
            "name": "Romans 7:2",
            "text": "A wife, for instance, whose husband is living is bound to him by the Law; but if her husband dies the law that bound her to him has now no hold over her."
        },
        {
            "chapter": 7,
            "verse": 3,
            "name": "Romans 7:3",
            "text": "This accounts for the fact that if during her husband's life she lives with another man, she will be stigmatized as an adulteress; but that if her husband is dead she is no longer under the old prohibition, and even though she marries again, she is not an adulteress."
        },
        {
            "chapter": 7,
            "verse": 4,
            "name": "Romans 7:4",
            "text": "So, my brethren, to you also the Law died through the incarnation of Christ, that you might be wedded to Another, namely to Him who rose from the dead in order that we might yield fruit to God."
        },
        {
            "chapter": 7,
            "verse": 5,
            "name": "Romans 7:5",
            "text": "For whilst we were under the thraldom of our earthly natures, sinful passions-- made sinful by the Law--were always being aroused to action in our bodily faculties that they might yield fruit to death."
        },
        {
            "chapter": 7,
            "verse": 6,
            "name": "Romans 7:6",
            "text": "But seeing that we have died to that which once held us in bondage, the Law has now no hold over us, so that we render a service which, instead of being old and formal, is new and spiritual."
        },
        {
            "chapter": 7,
            "verse": 7,
            "name": "Romans 7:7",
            "text": "What follows? Is the Law itself a sinful thing? No, indeed; on the contrary, unless I had been taught by the Law, I should have known nothing of sin as sin. For instance, I should not have known what covetousness is, if the Law had not repeatedly said, <FO>\"Thou shalt not covet.\"<Fo>"
        },
        {
            "chapter": 7,
            "verse": 8,
            "name": "Romans 7:8",
            "text": "Sin took advantage of this, and by means of the Commandment stirred up within me every kind of coveting; for apart from Law sin would be dead."
        },
        {
            "chapter": 7,
            "verse": 9,
            "name": "Romans 7:9",
            "text": "Once, apart from Law, I was alive, but when the Commandment came, sin sprang into life, and I died;"
        },
        {
            "chapter": 7,
            "verse": 10,
            "name": "Romans 7:10",
            "text": "and, as it turned out, the very Commandment which was to bring me life, brought me death."
        },
        {
            "chapter": 7,
            "verse": 11,
            "name": "Romans 7:11",
            "text": "For sin seized the advantage, and by means of the Commandment it completely deceived me, and also put me to death."
        },
        {
            "chapter": 7,
            "verse": 12,
            "name": "Romans 7:12",
            "text": "So that the Law itself is holy, and the Commandment is holy, just and good."
        },
        {
            "chapter": 7,
            "verse": 13,
            "name": "Romans 7:13",
            "text": "Did then a thing which is good become death to me? No, indeed, but sin did; so that through its bringing about death by means of what was good, it might be seen in its true light as sin, in order that by means of the Commandment the unspeakable sinfulness of sin might be plainly shown."
        },
        {
            "chapter": 7,
            "verse": 14,
            "name": "Romans 7:14",
            "text": "For we know that the Law is a spiritual thing; but I am unspiritual--the slave, bought and sold, of sin."
        },
        {
            "chapter": 7,
            "verse": 15,
            "name": "Romans 7:15",
            "text": "For what I do, I do not recognize as my own action. What I desire to do is not what I do, but what I am averse to is what I do."
        },
        {
            "chapter": 7,
            "verse": 16,
            "name": "Romans 7:16",
            "text": "But if I do that which I do not desire to do, I admit the excellence of the Law,"
        },
        {
            "chapter": 7,
            "verse": 17,
            "name": "Romans 7:17",
            "text": "and now it is no longer I that do these things, but the sin which has its home within me does them."
        },
        {
            "chapter": 7,
            "verse": 18,
            "name": "Romans 7:18",
            "text": "For I know that in me, that is, in my lower self, nothing good has its home; for while the will to do right is present with me, the power to carry it out is not."
        },
        {
            "chapter": 7,
            "verse": 19,
            "name": "Romans 7:19",
            "text": "For what I do is not the good thing that I desire to do; but the evil thing that I desire not to do, is what I constantly do."
        },
        {
            "chapter": 7,
            "verse": 20,
            "name": "Romans 7:20",
            "text": "But if I do that which I desire not to do, it can no longer be said that it is I who do it, but the sin which has its home within me does it."
        },
        {
            "chapter": 7,
            "verse": 21,
            "name": "Romans 7:21",
            "text": "I find therefore the law of my nature to be that when I desire to do what is right, evil is lying in ambush for me."
        },
        {
            "chapter": 7,
            "verse": 22,
            "name": "Romans 7:22",
            "text": "For in my inmost self all my sympathy is with the Law of God;"
        },
        {
            "chapter": 7,
            "verse": 23,
            "name": "Romans 7:23",
            "text": "but I discover within me a different Law at war with the Law of my understanding, and leading me captive to the Law which is everywhere at work in my body--the Law of sin."
        },
        {
            "chapter": 7,
            "verse": 24,
            "name": "Romans 7:24",
            "text": "(Unhappy man that I am! who will rescue me from this death-burdened body?"
        },
        {
            "chapter": 7,
            "verse": 25,
            "name": "Romans 7:25",
            "text": "Thanks be to God through Jesus Christ our Lord!) To sum up then, with my understanding, I--my true self--am in servitude to the Law of God, but with my lower nature I am in servitude to the Law of sin."
        }
    ]
}