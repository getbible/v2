{
  "translation": "Weymouth NT",
  "abbreviation": "weymouth",
  "lang": "en",
  "language": "English",
  "direction": "LTR",
  "encoding": "",
  "nr": 65,
  "name": "Jude",
  "chapters": [
    {
      "chapter": 1,
      "name": "Jude 1",
      "verses": [
        {
          "chapter": 1,
          "verse": 1,
          "name": "Jude 1:1",
          "text": "Jude, a bondservant of Jesus Christ and a brother of James: To those who are in God the Father, enfolded in His love, and kept for Jesus Christ, and called."
        },
        {
          "chapter": 1,
          "verse": 2,
          "name": "Jude 1:2",
          "text": "May mercy, peace and love be abundantly granted to you."
        },
        {
          "chapter": 1,
          "verse": 3,
          "name": "Jude 1:3",
          "text": "Dear friends, since I am eager to begin a letter to you on the subject of our common salvation, I find myself constrained to write and cheer you on to the vigorous defense of the faith delivered once for all to God's people."
        },
        {
          "chapter": 1,
          "verse": 4,
          "name": "Jude 1:4",
          "text": "For certain persons have crept in unnoticed--men spoken of in ancient writings as pre-destined to this condemnation--ungodly men, who pervert the grace of our God into an excuse for immorality, and disown Jesus Christ, our only Sovereign and Lord."
        },
        {
          "chapter": 1,
          "verse": 5,
          "name": "Jude 1:5",
          "text": "I desire to remind you--although the whole matter is already familiar to you--that the Lord saved a people out of the land of Egypt, but afterwards destroyed those who had no faith."
        },
        {
          "chapter": 1,
          "verse": 6,
          "name": "Jude 1:6",
          "text": "And angels--those who did not keep the position originally assigned to them, but deserted their own proper abode--He reserves in everlasting bonds, in darkness, in preparation for the judgement of the great day."
        },
        {
          "chapter": 1,
          "verse": 7,
          "name": "Jude 1:7",
          "text": "So also Sodom and Gomorrah--and the neighboring towns in the same manner--having been guilty of gross fornication and having gone astray in pursuit of unnatural vice, are now before us as a specimen of the fire of the Ages in the punishment which they are undergoing."
        },
        {
          "chapter": 1,
          "verse": 8,
          "name": "Jude 1:8",
          "text": "Yet in just the same way these dreamers also pollute the body, while they set authority at naught and speak evil of dignities."
        },
        {
          "chapter": 1,
          "verse": 9,
          "name": "Jude 1:9",
          "text": "But Michael the Archangel, when contending with the Devil and arguing with him about the body of Moses, did not dare to pronounce judgement on him in abusive terms, but simply said, \"The Lord rebuke you.\""
        },
        {
          "chapter": 1,
          "verse": 10,
          "name": "Jude 1:10",
          "text": "Yet these men are abusive in matters of which they know nothing, and in things which, like the brutes, they understand instinctively--in all these they corrupt themselves."
        },
        {
          "chapter": 1,
          "verse": 11,
          "name": "Jude 1:11",
          "text": "Alas for them; for they have followed in the steps of Cain; for the sake of gain they have rushed on headlong in the evil ways of Balaam; and have perished in rebellion like that of Korah!"
        },
        {
          "chapter": 1,
          "verse": 12,
          "name": "Jude 1:12",
          "text": "These men--sunken rocks! --are those who share the pleasure of your love-feasts, unrestrained by fear while caring only for themselves; clouds without water, driven away by the winds; trees that cast their fruit, barren, doubly dead, uprooted;"
        },
        {
          "chapter": 1,
          "verse": 13,
          "name": "Jude 1:13",
          "text": "wild waves of the sea, foaming out their own shame; wandering stars, for whom is reserved dense darkness of age-long duration."
        },
        {
          "chapter": 1,
          "verse": 14,
          "name": "Jude 1:14",
          "text": "It was also about these that Enoch, who belonged to the seventh generation from Adam, prophesied, saying, \"The Lord has come, attended by myriads of His people, to execute judgement upon all,"
        },
        {
          "chapter": 1,
          "verse": 15,
          "name": "Jude 1:15",
          "text": "and to convict all the ungodly of all the ungodly deeds which in their ungodliness they have committed, and of all the hard words which they, ungodly sinners as they are, have spoken against Him.\""
        },
        {
          "chapter": 1,
          "verse": 16,
          "name": "Jude 1:16",
          "text": "These men are murmurers, ever bemoaning their lot. Their lives are guided by their evil passions, and their mouths are full of big, boastful words, while they treat individual men with admiring reverence for the sake of the advantage they can gain."
        },
        {
          "chapter": 1,
          "verse": 17,
          "name": "Jude 1:17",
          "text": "But as for you, my dearly-loved friends, remember the words that before now were spoken by the Apostles of our Lord Jesus Christ--"
        },
        {
          "chapter": 1,
          "verse": 18,
          "name": "Jude 1:18",
          "text": "how they declared to you, \"In the last times there shall be scoffers, obeying only their own ungodly passions.\""
        },
        {
          "chapter": 1,
          "verse": 19,
          "name": "Jude 1:19",
          "text": "These are those who cause divisions. They are men of the world, wholly unspiritual."
        },
        {
          "chapter": 1,
          "verse": 20,
          "name": "Jude 1:20",
          "text": "But you, my dearly-loved friends, building yourselves up on the basis of your most holy faith and praying in the Holy Spirit,"
        },
        {
          "chapter": 1,
          "verse": 21,
          "name": "Jude 1:21",
          "text": "must keep yourselves safe in the love of God, waiting for the mercy of our Lord Jesus Christ which will result in the Life of the Ages."
        },
        {
          "chapter": 1,
          "verse": 22,
          "name": "Jude 1:22",
          "text": "Some, when they argue with you, you must endeavor to convince;"
        },
        {
          "chapter": 1,
          "verse": 23,
          "name": "Jude 1:23",
          "text": "others you must try to save, as brands plucked from the flames; and on others look with pity mingled with fear, while you hate every trace of their sin."
        },
        {
          "chapter": 1,
          "verse": 24,
          "name": "Jude 1:24",
          "text": "But to Him who is able to keep you safe from stumbling, and cause you to stand in the presence of His glory free from blemish and full of exultant joy--"
        },
        {
          "chapter": 1,
          "verse": 25,
          "name": "Jude 1:25",
          "text": "to the only God our Saviour--through Jesus Christ our Lord, be ascribed glory, majesty, might, and authority, as it was before all time, is now, and shall be to all the Ages! Amen."
        }
      ]
    }
  ]
}
